package src;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Station;
import model.vo.Trip;

public class Tests {

	private IStack stackTrips;
	private IQueue queueTrips;

	private Station estacion= new Station("5","","","","","","");
	@Before
	public void crearLista(){
		stackTrips = new Stack();
		queueTrips = new Queue();
		
		
		stackTrips.push(new Station("2","","","","","",""));
		stackTrips.push(new Station("3","","","","","",""));
		stackTrips.push(new Station("4","","","","","",""));
		stackTrips.push(new Station("1","","","","","",""));
		stackTrips.push(new Station("6","","","","","",""));
		stackTrips.push(new Station("7","","","","","",""));
		stackTrips.push(new Station("8","","","","","",""));
		stackTrips.push(estacion);
		
		queueTrips.enqueue(estacion);
		queueTrips.enqueue(new Station("1","","","","","",""));
		queueTrips.enqueue(new Station("2","","","","","",""));
		queueTrips.enqueue(new Station("3","","","","","",""));
		queueTrips.enqueue(new Station("4","","","","","",""));
		queueTrips.enqueue(new Station("6","","","","","",""));
		queueTrips.enqueue(new Station("7","","","","","",""));
		queueTrips.enqueue(new Station("8","","","","","",""));
		
	}
	@Test
	public void darTamano() {
		assertTrue("El tama�o de la lista no es el correcto",stackTrips.size()==8);
	}
	@Test
	public void pop() {
		
		assertTrue("El elemento no se elimin� correctamente",stackTrips.pop().equals(estacion));
	}
	@Test
	public void dequeue() {
		assertTrue("El elemento no se elimin� correctamente",queueTrips.dequeue().equals(estacion));
	}

	@Test
	public void vacio() {
		assertTrue("El elemento no se a�adi� en la posici�n correcta",stackTrips.isEmpty()==false);
	}
	@Test
	public void delete() {
		
		assertTrue("El elemento no se elimin� correctamente",queueTrips.isEmpty()==false);
		
	}
}
