package model.logic;

import api.IDivvyTripsManager;
import model.vo.Station;
import model.vo.Trip;
import model.vo.VOTrip;

import java.io.FileReader;
import java.util.Iterator;

import model.data_structures.*;

import com.opencsv.CSVReader;
import com.sun.xml.internal.bind.v2.runtime.reflect.ListIterator;


public class DivvyTripsManager implements IDivvyTripsManager {

	private IDoublyLinkedList stations;
	private IDoublyLinkedList trips;
	private IStack stackTrips;
	private IQueue queueTrips;
	
	
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		try{
			CSVReader lector = new CSVReader(new FileReader(stationsFile));
			String[] l;
			int contador = 0;
			l= lector.readNext();
			l = lector.readNext();
			stations = new DoubleLinkedList(new Node(new Station(l[0], l[1], l[2], l[3], l[4], l[5], l[6])));
			while((l = lector.readNext())!=null){
				contador++;
				
				stations.anadirNodo(new Node(new Station(l[0], l[1], l[2], l[3], l[4], l[5], l[6])));
				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		try{
			CSVReader lector = new CSVReader(new FileReader(tripsFile));
			String[] l;
			l=lector.readNext();
			l = lector.readNext();
			int contador = 0;
			//trips = new DoubleLinkedList(new Node(new Trip(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8], l[9], l[10], l[11])));
			stackTrips = new Stack();
			queueTrips = new Queue();
			stackTrips.push(new Trip(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8], l[9], l[10], l[11]));
			queueTrips.enqueue(new Trip(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8], l[9], l[10], l[11]));	
			while((l = lector.readNext()) != null){
				contador++;
				stackTrips.push(new Trip(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8], l[9], l[10], l[11]));
				queueTrips.enqueue(new Trip(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8], l[9], l[10], l[11]));
				//trips.anadirNodo(new Node(new Trip(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7], l[8], l[9], l[10], l[11])));
				
				
			}
			System.out.println(stackTrips.darPrimero().darElemento().toString());
			System.out.println(queueTrips.darPrimero().darElemento().toString());
			System.out.println(stackTrips.darUltimo().darElemento().toString());
			System.out.println(queueTrips.darUltimo().darElemento().toString());
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		// TODO Auto-generated method stub
		Node actual = stackTrips.darPrimero();
		IDoublyLinkedList<String> retornar = new DoubleLinkedList<>(null); 
		int contador = 0;
		while(actual.darSig()!= null && n >0) {
			String[] info = actual.darElemento().toString().split(",");
			if(info[3].equals(bicycleId + "")) {
				String linea = info[8];
				retornar.anadirNodo(new Node(linea));
				n--;
			}
			actual = actual.darSig();
		}
		return retornar;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		// TODO Auto-generated method stub
		Node actual = queueTrips.darPrimero();
		VOTrip resp = null;
		int contador = 0;
		while(actual.darSig()!=null && n>0) {
			contador++;
			String[] info = actual.darElemento().toString().split(",");
			if(info[7].equals(stationID+"")) {
				n--;
				if(n==0) {
					resp = new VOTrip(info[0], info[4], info[6], info[8]);
				}
			}
			
			actual = actual.darSig();
		}
		return resp;
	}	


}
