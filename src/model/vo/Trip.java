package model.vo;

public class Trip {

	private String id;					//0
	private String startTime;			//1
	private String endTime;				//2
	private String bikeId;				//3
	private String tripDuration;		//4
	private String startStationId;		//5
	private String startStationName;	//6
	private String endStationId;		//7
	private String endStationName;		//8
	private String userType;			//9
	private String gender;				//10
	private String birthyear;			//11
	
	public Trip(String pId, String pStartTime, String pEndTime, String pBikeId, String pTripDuration, String pStartStationId,String pStartStationName, String pEndStationId, String pEndStationName, String pUserType, String pGender,String pBirthyear) {
		id = pId;
		startTime = pStartTime;
		endTime = pEndTime;
		bikeId = pBikeId;
		tripDuration = pTripDuration;
		startStationId = pStartStationId;
		startStationName = pStartStationName;
		endStationId = pEndStationId;
		endStationName = pEndStationName;
		userType=pUserType;
		gender = pGender;
		birthyear = pBirthyear;
	}
	/**
	 * Metodo para comparar
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return id+","+startTime+","+endTime+","+bikeId+","+tripDuration+","+startStationId+","+startStationName+","+endStationId+","+endStationName+","+userType+","+gender+","+birthyear;
	}
	
	public String elCoso() {
		return id+" "+tripDuration+" "+startStationName+" "+endStationName+'\n';
	}
	
	public String darId() {
		return id;
	}
	public String darStartTime() {
		return startTime;
	}
	public String darEndTime() {
		return endTime;
	}
	public String darBikeId() {
		return bikeId;
	}
	public String darTripDuration() {
		return tripDuration;
	}
	public String darStartStationId() {
		return startStationId;
	}
	public String darStartStationName() {
		return startStationName;
	}
	public String darEndStationId() {
	return endStationId;
	}
	public String darEndStationName() {
		return endStationName;
	}
	public String darUserType() {
		return userType;
	}
	public String darGender() {
		return gender;
	}
	public String darBirthYear() {
		return birthyear;
	}
	
}
