package model.vo;

public class Station {

	private String id;
	private String name;
	private String city;
	private String latitude;
	private String longitude;
	private String dpCapacity;
	private String onlineDate;
	
	public Station(String pId, String pName, String pCity, String pLat, String pLong, String pDpC, String onDate) {
		id = pId;
		name = pName;
		city = pCity;
		latitude = pLat;
		longitude = pLong;
		dpCapacity = pDpC;
		onlineDate = onDate;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return id+", "+name+", "+city+", "+latitude+", "+longitude+", "+dpCapacity+", "+onlineDate;
	}
}
