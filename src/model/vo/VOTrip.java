package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	
	private String id;
	private String time;
	private String fromStation;
	private String toStation;
	
	public VOTrip(String pId, String pTim, String from, String to) {
		id =pId;
		time = pTim;
		fromStation = from;
		toStation = to;
	}
	/**
	 * @return id - Trip_id
	 */
	public String id() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public String getTripSeconds() {
		// TODO Auto-generated method stub
		return time;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return fromStation;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return toStation;
	}
}
