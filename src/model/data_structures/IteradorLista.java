package model.data_structures;

import java.io.Serializable;
import java.util.ListIterator;

/**
 * Clase que representa el iterador de lista (avanza hacia adelante y hacia atr�s)
 * @param <E> Tipo del objeto que almacena el iterador de la lista
 */
public class IteradorLista<E> implements ListIterator<E>, Serializable 
{

	/**
	 * Constante de serializaci�n
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nodo anterior al que se encuentra el iterador.
	 */
	private Node<E> anterior;

	/**
	 * Nodo en el que se encuentra el iterador.
	 */
	private Node<E> actual;

	/**
	 * Crea un nuevo iterador de lista iniciando en el nodo que llega por par�metro
	 * @param primerNodo el nodo en el que inicia el iterador. nActual != null
	 */
	public IteradorLista(Node<E> primerNodo)
	{
		actual = primerNodo;
		anterior = null;
	}

	/**
	 * Indica si hay nodo siguiente
	 * true en caso que haya nodo siguiente o false en caso contrario
	 */
	public boolean hasNext() 
	{
		// TODO Completar seg�n la documentaci�n
		return actual.darSig()!=null;
	}

	/**
	 * Indica si hay nodo anterior
	 * true en caso que haya nodo anterior o false en caso contrario
	 */
	public boolean hasPrevious() 
	{
		// TODO Completar seg�n la documentaci�n
		return actual.darAnt()!=null;
	}

	/**
	 * Devuelve el elemento siguiente de la iteraci�n y avanza.
	 * @return elemento siguiente de la iteraci�n
	 */
	public E next() 
	{
		// TODO Completar seg�n la documentaci�n
		E elemento=actual.darSig().darElemento();
		actual=(Node<E>) actual.darSig();
		return elemento;
	}

	/**
	 * Devuelve el elemento anterior de la iteraci�n y retrocede.
	 * @return elemento anterior de la iteraci�n.
	 */
	public E previous() 
	{
		// TODO Completar seg�n la documentaci�n
		E elemento=actual.darAnt().darElemento();
		actual=actual.darAnt();
		return elemento;
	}


	//=======================================================
	// M�todos que no se implementar�n
	//=======================================================

	public int nextIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public int previousIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public void remove() 
	{
		throw new UnsupportedOperationException();
	}

	public void set(E e) 
	{
		throw new UnsupportedOperationException();
	}

	public void add(E e) 
	{
		throw new UnsupportedOperationException();		
	}

}
