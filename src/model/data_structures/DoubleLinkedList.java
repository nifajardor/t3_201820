package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T> implements IDoublyLinkedList<T>{
	

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return tamanio;
	}
	protected Node<T> primero;
	protected int tamanio;
	protected Node<T> ultimo;
	
	public DoubleLinkedList(Node<T> pPrimero) {
		primero = pPrimero;
		ultimo= pPrimero;
		tamanio = 0;
	}
	
	public Node<T> darPrimero() {
		return primero;
	}
	public Node<T> darUltimo() {
		return ultimo;
	}
	@Override
	public void anadirNodo(Node<T> pAnadir) {
		if(primero != null){
			primero.cambiarAnt(pAnadir);
			pAnadir.cambiarSig(primero);
			
			primero = pAnadir;
			tamanio++;
		}
		else{
			primero = pAnadir;
			ultimo=pAnadir;
			tamanio++;
		}
	}
	public void anadirNodoAlFinal(Node<T> pAnadir, Node<T> pExistente) {
		if(primero != null) {
			pExistente.cambiarSig(pAnadir);
			pAnadir.cambiarAnt(pExistente);
			tamanio++;
			ultimo = pAnadir;
		}
		else {
			primero = pAnadir;
			ultimo = pAnadir;
			tamanio++;
		}
		
	}
	
	public boolean vacio() {
		// TODO Auto-generated method stub
		if(primero==null){
			return true;
		}else{
			return false;
		}
	}
	public T eliminarPrimero() {
		T retorno;
		Node<T> actual=(Node<T>) primero;
		if(primero != null){
			retorno=primero.darElemento();
			actual=actual.darSig();
			if(actual!=null){
				actual.cambiarAnt(null);
			}
			primero=actual;
			
			tamanio--;
			return retorno;
		}
		return null;
	}
	
	public T eliminarUltimo() {
		T retorno;
		Node<T> actual=(Node<T>) ultimo;
		if(ultimo != null){
			retorno=ultimo.darElemento();
			actual=actual.darAnt();
			actual.cambiarSig(null);
			ultimo.cambiarAnt(null);
			ultimo=actual;
			tamanio--;
			return retorno;
		}
		return null;
	}
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorLista<T> (primero);
	}

}
