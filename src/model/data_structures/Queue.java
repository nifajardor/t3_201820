package model.data_structures;

public class Queue<T> extends DoubleLinkedList<T> implements IQueue<T>{

	public Queue() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return vacio();
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return tamanio;
	}
	public Node<T> darPrimero(){
		return primero;
	}
	public Node<T> darUltimo(){
		return ultimo;
	}

	@Override
	public void enqueue(T t) {
		// TODO Auto-generated method stub
		anadirNodoAlFinal(new Node<T>(t), ultimo);
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		return eliminarPrimero();
	}

}
