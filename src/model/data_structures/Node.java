package model.data_structures;

public class Node<E> {

	private Node<E> siguiente;
	private Node<E> anterior;
	private E elemento;
	
	public Node(E pElem) {
		elemento = pElem;
		anterior = null;
		siguiente = null;
	}
	
	public E darElemento() {
		return elemento;
	}
	public Node<E> darSig() {
		return siguiente;
	}
	public Node<E> darAnt() {
		return anterior;
	}
	public void cambiarSig(Node<E> pSig) {
		siguiente = pSig;
	}
	public void cambiarAnt(Node<E> pAnt) {
		anterior = pAnt;
	}
	public void imprimirr() {
		System.out.println(elemento.toString());
	}
	
}
